<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TreeViewController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/data',[TreeViewController::class,'data'])->name('data');
Route::get('/home',[TreeViewController::class,'home'])->name('home');
Route::get('/showView/{id}',[TreeViewController::class,'showView'])->name('showView');
Route::post('/show/{id}',[TreeViewController::class,'show'])->name('show');
Route::get('/create/{id}',[TreeViewController::class,'create'])->name('create');
Route::post('/create/{id}',[TreeViewController::class,'create_post'])->name('create_post');
Route::post('/addTreeview',[TreeViewController::class,'addTreeview'])->name('addTreeview');
Route::post('/edit', [TreeViewController::class, 'edit'])->name('edit');
Route::post('/isdelete', [TreeViewController::class, 'isdelete'])->name('isdelete');

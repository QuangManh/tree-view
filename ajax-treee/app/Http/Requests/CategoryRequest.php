<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'email' => 'required|email:rfc,dns:',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Không được để trống tiêu đề',
            'age.required' => 'Không được để trống tuổi',
            'age.number' => 'Phải nhập số',
            'gender.required' => 'Không được để trống giới tính',
            'email.required' => 'Không được để trống email',
            'email.email' => 'Email ko đúng định dạng',
        ];
    }
}

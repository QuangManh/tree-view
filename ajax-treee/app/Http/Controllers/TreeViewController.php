<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;

class TreeViewController extends Controller
{
    //

    public function data(Request $request)
    {
        $data = Category::where('parent_id', '=', 0)
            ->where('isdelete', 1)->get();

        $tree = '<ul id="myUL">';
        foreach ($data as $item) {
            if ($item->isdelete == 1) {
                $tree .= '<li><span class="caret"><a href="http://manh.local/showView/' . $item->id . '">' . $item->title . '</a></span><a href="http://manh.local/create/' . $item->id . '">+</a>';
                if (count($item->childs)) {
                    $tree .= $this->childView($item);
                }
            }
        }
        $tree .= '<ul>';
        return response()->json([
            'data' => $data,
            'status' => 200,
            'tree' => $tree,
        ]);
    }

    public function childView($items)
    {

        $html = '<ul class="nested">';
        foreach ($items->childs as $item) {
            if ($item->isdelete == 1) {
                if (count($item->childs)) {
                    $html .= '<li><span class="caret"><a href="http://manh.local/showView/' . $item->id . '">' . $item->title . '</a></span><a href="http://manh.local/create/' . $item->id . '">+</a>';
                    $html .= $this->childView($item);
                } else {
                    $html .= '<li>' . $item->title . '<a href="http://manh.local/create/' . $item->id . '">+</a></li>';
                    $html .= '</li>';
                }
            }
        }
        $html .= "</ul>";
        return $html;
    }

    public function home()
    {
        return view('home');
    }

    public function create_post(Request $request)
    {
        Category::create([
            'title' => $request->title,
            'parent_id' => $request->id,
            'age' => $request->age,
            'gender' => $request->gender,
            'email' => $request->email,
        ]);
        return response()->json([
            'status' => 200,
        ]);
    }

    public function create($id)
    {
        return view('create', compact('id'));
    }

    public function showView()
    {
        return view('show');
    }

    public function show(Request $request)
    {
        //truy vấn tìm cha
        $parent = Category::where('id', $request->id)->first();

        //tập con
        $data = $parent->childs;

        $html = '';

        if (empty($request->title)) {
            foreach ($data as $item) {
                if ($item->isdelete == 1) {
                    $gender = 'Nam';
                    if ($item->gender == 2) {
                        $gender = 'Nữ';
                    }
                    if (count($item->childs)) {
                        $html .= '
                        <tr>
                            <td>' . $item->id . '</td>
                            <td><a href="http://manh.local/showView/' . $item->id . '">' . $item->title . '</a></td>
                            <td><a>' . $item->email . '</a></td>
                            <td><a>' . $gender . '</a></td>
                            <td><a class="btn btn-primary" href="http://manh.local/create/' . $item->id . '">+</a></td>
                            <td>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modelId' . $item->id . '">
                                    sửa
                                </button>
                            
                            ';
                        $html .= $this->modal($item->id, $item->title, $item->age, $item->gender, $item->email);
                        $html .= '
                        </td>
                            <td><a class="isdelete btn btn-danger" data-a="' . $item->id . '">xóa</a></td>
                        </tr>
                    ';
                    } else {
                        $html .= '
                        <tr>
                            <td>' . $item->id . '</td>
                            <td>' . $item->title . '</td>
                            <td>' . $item->email . '</td>
                            <td>' . $gender . '</td>
                            <td><a class="btn btn-primary" href="http://manh.local/create/' . $item->id . '">+</a></td>
                            <td>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modelId' . $item->id . '">
                                    sửa
                                </button>
                            
                        ';
                        $html .= $this->modal($item->id, $item->title, $item->age, $item->gender, $item->email);
                        $html .= '
                        </td>
                            <td><a class="isdelete btn btn-danger" data-a="' . $item->id . '">xóa</a></td>
                        </tr>
                    ';
                    }
                }
            }
        } else {
            $dataSearch = Category::where('parent_id', $request->id)
                ->where('isdelete', 1)
                ->where('title', 'like', '%' . $request->title . '%')->get();
            foreach ($dataSearch as $item) {
                if (count($item->childs)) {
                    $html .= '
                    <tr>
                        <td>' . $item->id . '</td>
                        <td><a href="http://manh.local/showView/' . $item->id . '">' . $item->title . '</a></td>
                        <td><a class="btn btn-primary" href="http://manh.local/create/' . $item->id . '">+</a></td>
                        <td>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modelId' . $item->id . '">
                                sửa
                            </button>
                        
                        ';
                    $html .= $this->modal($item->id, $item->title, $item->age, $item->gender, $item->email);
                    $html .= '
                    </td>
                        <td><a class="isdelete btn btn-danger" data-a="' . $item->id . '">xóa</a></td>
                    </tr>
                ';
                } else {
                    $html .= '
                    <tr>
                        <td>' . $item->id . '</td>
                        <td>' . $item->title . '</td>
                        <td><a class="btn btn-primary" href="http://manh.local/create/' . $item->id . '">+</a></td>
                        <td>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modelId' . $item->id . '">
                                sửa
                            </button>
                        
                    ';
                    $html .= $this->modal($item->id, $item->title, $item->age, $item->gender, $item->email);
                    $html .= '
                    </td>
                        <td><a class="isdelete btn btn-danger" data-a="' . $item->id . '">xóa</a></td>
                    </tr>
                ';
                }
            }
        }

        return response()->json([
            'data' => $html,
            'status' => 200,
        ]);
    }

    public function modal($id, $title, $age, $gender, $email)
    {
        $op = '';
        if ($gender == 1) {
            $op .= '<option selected value="1">Nam</option>
            <option value="2">Nữ</option>';
        } else {
            $op .= '<option value="1">Nam</option>
            <option selected value="2">Nữ</option>';
        }
        $html = '
            <!-- Button trigger modal -->


            <!-- Modal -->
            <div class="modal fade" id="modelId' . $id . '" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Sửa tiêu đề</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="form-group">
                                    <label for="">Nhập tiêu đề</label>
                                    <input type="text" class="data_title' . $id . ' form-control" name="" value="' . $title . '" id="" aria-describedby="emailHelpId"
                                        placeholder="Nguyễn Văn A">
                                    <input type="hidden" class="id' . $id . ' form-control" name="" value="' . $id . '" id="" aria-describedby="emailHelpId"
                                        placeholder="Nguyễn Văn A">
                                </div>
                                <div class="form-group">
                                    <label for="">Nhập Tuổi</label>
                                    <input type="text" class="data_age' . $id . ' form-control" name="" value="' . $age . '" id="" aria-describedby="emailHelpId"
                                        placeholder="Nguyễn Văn A">
                                </div>
                                <div class="form-group">
                                    <label for="">Giới tính</label>
                                    <select class="data_gender' . $id . ' form-control" name="" id="">
                                        ' . $op . '
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" class="data_email' . $id . ' form-control" name="" value="' . $email . '" id="" aria-describedby="emailHelpId"
                                        placeholder="Nguyễn Văn A">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="button" data-id-edit="' . $id . '" class="edit btn btn-primary" data-dismiss="modal">Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
        ';
        return $html;
    }

    public function edit(Request $request)
    {
        Category::where('id', $request->id)->update(['title' => $request->title, 'age' => $request->age, 'gender' => $request->gender, 'email' => $request->email]);
        return response()->json([
            'status' => 200,
        ]);
    }

    public function isdelete(Request $request)
    {
        Category::where('id', $request->id)->update(['isdelete' => 2]);
        return response()->json([
            'status' => 200,
        ]);
    }
}

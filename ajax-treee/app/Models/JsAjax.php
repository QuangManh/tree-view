<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JsAjax extends Model
{
    use HasFactory;
    protected $table = 'js_ajax';
    protected $fillable = [''];
}

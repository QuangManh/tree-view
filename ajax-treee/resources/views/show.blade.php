<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <a href="{{route('home')}}" class="btn btn-primary">Quay về trang chủ</a>
    <div class="container">
        <br>
        <div class="input-group mb-3">
            <input type="text" class=" title form-control" placeholder="Tìm kiếm..." aria-label="Recipient's username"
                aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="search btn btn-outline-secondary" type="button">Tìm Kiếm</button>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Tên</th>
                    <th>Thêm phần con</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="data-table">

            </tbody>
        </table>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);
            let data = {
                id: id,
            }
            load(data);
            $('.search').click(function(e) {
                let title = $('.title').val();
                data.title = title;
                load(data);
            });
        });

        function load(data) {
            $.ajax({
                type: "POST",
                url: "http://manh.local/show" + '/' + data.id,
                data: {
                    id: data.id,
                    title: data.title,
                },
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(response) {
                    $('.data-table').html(response.data);
                    $('.edit').click(function(e) {
                        let id_edit = $(this).attr("data-id-edit");
                        let data_title = $('.data_title'+id_edit).val();
                        let data_id = $('.id'+id_edit).val()
                        let data_age = $('.data_age'+id_edit).val()
                        let data_gender = $('.data_gender'+id_edit).val()
                        let data_email = $('.data_email'+id_edit).val()
                        data.id = data_id;
                        data.title = data_title;
                        data.age = data_age;
                        data.gender = data_gender;
                        data.email = data_email;
                        edit(data);
                    });

                    $('.isdelete').click(function(e) {
                        let isdelete_id = $(this).attr('data-a')
                        data.id = isdelete_id;
                        isdelete(data);
                    });

                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        function edit(data_edit) {
            $.ajax({
                type: "post",
                url: "http://manh.local/edit",
                data: {
                    id: data_edit.id,
                    title: data_edit.title,
                    age:data_edit.age,
                    gender:data_edit.gender,
                    email:data_edit.email,
                },
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(response) {
                    let url = window.location.pathname;
                    let id = url.substring(url.lastIndexOf('/') + 1);
                    let data_load = {
                        id: id,
                    }
                    load(data_load);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        function isdelete(data_isdelete) {
            console.log(data_isdelete.id);
            $.ajax({
                type: "post",
                url: "http://manh.local/isdelete",
                data: {
                    id: data_isdelete.id,
                },
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(response) {
                    let url = window.location.pathname;
                    let id = url.substring(url.lastIndexOf('/') + 1);
                    let data_load = {
                        id: id,
                    }
                    load(data_load);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    </script>
</body>

</html>

<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <form action="" method="">

            <div class="form-group">
                <label for="">Tiêu đề</label>
                <input type="text" class="form-control" name="title" id="title" aria-describedby="emailHelpId"
                    placeholder="">
                <input type="hidden" class="form-control" name="id" id="id" value="{{ $id }}">
            </div>
            <div class="form-group">
                <label for="">Tuổi</label>
                <input type="text" class="age form-control" name="title" id="title" aria-describedby="emailHelpId"
                    placeholder="">
            </div>
            <div class="form-group">
                <label for="">Giới tính</label>
                <select class="gender form-control" name="" id="">
                    <option selected value="1">Nam</option>
                    <option value="2">Nữ</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Email</label>
                <input type="email" class="email form-control" name="title" id="title" aria-describedby="emailHelpId"
                    placeholder="">
                <small class="emailError"></small>
            </div>

            <button type="button" class="add btn btn-primary">Thêm</button>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $('.add').click(function(e) {
            let title = $('#title').val();
            let id = $('#id').val();
            let age = $('.age').val();
            let gender = $('.gender').val();
            let email = $('.email').val();
            $.ajax({
                type: "POST",
                url: "http://manh.local/create" + '/' + id,
                data: {
                    title: title,
                    id: id,
                    age: age,
                    gender: gender,
                    email: email,
                },
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(response) {
                    window.location.href = "{{ Route('home') }}";
                }
            });
        });
    </script>
</body>

</html>
